package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author xx
 * @email xx@gmail.com
 * @date 2021-05-15 19:32:09
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
