package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * ����
 * 
 * @author xx
 * @email xx@gmail.com
 * @date 2021-05-15 17:20:14
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
