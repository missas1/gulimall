package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * ��Ʒ��������
 * 
 * @author xxxx
 * @email xxxx@gmail.com
 * @date 2021-05-08 17:31:47
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
