package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Ʒ��
 * 
 * @author xx
 * @email xx@gmail.com
 * @date 2021-05-13 19:20:54
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
