package com.atguigu.gulimall.member.feign;


import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 这是一个声明式的远程调用
 */
@FeignClient("gulimall-coupon")  // 声明此接口为一个远程客户端; 所调用的服务名与注册中心Nacos中所注册的被调用服务名一致
public interface CouponFeignService {

    @RequestMapping("/coupon/coupon/memeber/list")
    public R membercoupons();
}
